class Validator
  private

  def has_line_started_with?(gemfile_content, word)
    gemfile_content.each_line do |line|
      if line.start_with?(word)
        return true
      end
    end
    return false
  end

  def has_source?(gemfile_content)
    return has_line_started_with?(gemfile_content, "source")
  end

  def has_ruby_version?(gemfile_content)
    return has_line_started_with?(gemfile_content, "ruby")
  end

  def extract_gem(line)
    first_marker = "'"
    second_marker = "'"
    return line[/#{first_marker}(.*?)#{second_marker}/m, 1]
  end
  
  def has_ordered_gems?(gemfile_content)
    last_gem = ""
    gemfile_content.each_line do |line|
      if line.index("gem") == 0
        gem_name = extract_gem(line)
        if last_gem > gem_name
          return false
        end
        last_gem = gem_name
      elsif not (line.start_with?("ruby") or line.start_with?("source") or line.strip.empty?)
        return false
      end
    end
    return true
  end

  public

  def process(gemfile_content)
    if gemfile_content.empty?
      raise 'Error: Gemfile vacio'
    end
    if !has_source?(gemfile_content)
      raise 'Error: Gemfile sin source'
    end
    if !has_ruby_version?(gemfile_content)
      raise 'Error: Gemfile sin version' 
    end
    if !has_ordered_gems?(gemfile_content)
      raise 'Error: Gemfile unordered'
    end
    return true
  end

end
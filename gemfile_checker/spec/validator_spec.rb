require 'rspec'
require_relative '../model/validator'

describe Validator do

  subject { @validator = Validator.new }

  it { should respond_to (:process) }

  it 'should validate gemfile string and return boolean' do
    gemfile_string = ''
    begin
      result = subject.process gemfile_string
    rescue StandardError => e
      expect(e.message).to eq('Error: Gemfile vacio')
    end
  end

  it "should return raise a 'Gemfile sin source' error when a Gemfile dosen't have source" do
    gemfile_content = File.read "#{__dir__}/samples/Gemfile.invalid.source"
    begin
      validation_result = Validator.new.process gemfile_content
    rescue StandardError => e
      expect(e.message).to eq('Error: Gemfile sin source')
    end
  end

  it "should return raise a 'Gemfile sin version' error when a Gemfile dosen't have ruby" do
    gemfile_content = File.read "#{__dir__}/samples/Gemfile.invalid.ruby"
    begin
      validation_result = Validator.new.process gemfile_content
    rescue StandardError => e
      expect(e.message).to eq('Error: Gemfile sin version')
    end
  end

  it "should return raise a 'Gemfile unordered' error when Gemfile gems are not ordered alphabetically" do
    gemfile_content = File.read "#{__dir__}/samples/Gemfile.invalid.gems"
    begin
      validation_result = Validator.new.process gemfile_content
    rescue StandardError => e
      expect(e.message).to eq('Error: Gemfile unordered')
    end
  end

  it 'should return true when valid Gemfile' do
    gemfile_content = File.read  "#{__dir__}/samples/Gemfile.valid"
    validation_result = Validator.new.process gemfile_content
    expect(validation_result).to eq true
  end
end

